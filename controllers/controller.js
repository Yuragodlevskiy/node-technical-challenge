const Counter = require('../models/Counter');
const requests = require('../utils/requests');

exports.getDouble = async (req, res) => {
    try {
        const { number } = req.params;
        if (isNaN(+number)) {
            return res.status(400).send('Incorrect number format');
        } else {
            return res.send({
                result: number*2
            });
        }
    } catch (e) {
        res.send(e);
    }
}
exports.getCount = async (req, res) => {
    try {
        const counterArr = await Counter.find({});
        if (counterArr.length) {
            const { id } = counterArr[0];
            const counter = await Counter.findById(id);
            counter.count++;
            await counter.save();
            return res.send({
                count: counter.count
            });
        } else {
            const counter = new Counter({
                count: 1
            });
            await counter.save();
            return res.send({
                count: counter.count
            });
        }
    } catch (e) {
        res.send(e);
    }
}
exports.getJoke = async (req, res) => {
    try {
        const data = await requests.get('https://api.jokes.one/jod');
        if (data.error) {
            const { code, message } = data.error;
            return res.status(code).send(message);
        } else {
            return res.send(data.contents.jokes[0].joke.text);
        }
    } catch (e) {
        res.send(e);
    }
}
