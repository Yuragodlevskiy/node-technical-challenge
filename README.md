# Setup
1.create .env

2.fill .env

### If you`re using yarn
3.yarn

4.yarn dev
### If you`re using npm
3.npm i

4.npm run dev
