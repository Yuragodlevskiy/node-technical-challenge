const express = require('express');
const router = express.Router();
const scrapperController = require('../controllers/controller');

router.get('/double/:number', scrapperController.getDouble);
router.get('/count', scrapperController.getCount);
router.get('/joke', scrapperController.getJoke);

module.exports = router;
