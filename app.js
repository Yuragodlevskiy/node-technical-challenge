const express = require('express');
require('dotenv').config();
require('./utils/mongo-connection')();

const indexRouter = require('./routes/index');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', indexRouter);
module.exports = app;
