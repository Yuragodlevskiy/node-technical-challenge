const mongoose = require('mongoose');

module.exports = async function() {
    try {
        await mongoose.connect(
            process.env.MONGO_URL
        )
    } catch (e) {
        console.error(e)
    }
};
