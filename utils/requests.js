const https = require('https');
exports.get = (url) => new Promise((resolve, reject) => {
    https.get(url, (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
            data += chunk;
        });
        resp.on('end', () => {
            resolve(JSON.parse(data));
        });
        resp.on('error', () => {
            reject(JSON.parse(data));
        });
    });
});
